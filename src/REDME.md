// поднятие базы данных
docker run --name local_postgres -e POSTGERS_PASSWORD=test -p 5433:5432 postgres

// создание тура
curl --location 'http://localhost:8080/api/v1/tours' \
--header 'Content-Type: application/json' \
--data '{
"name": "test",
"destination": "test",
"arrivalDate": "2023-05-26T22:45:35.735957",
"departureDate": "2023-05-26T22:45:35.735957",
"ownerName": "Vitya",
"ownerSurname": "Top",
"ownerAge": 18
}'

// обновление тура
curl --location 'http://localhost:8080/api/v1/tours/4' \
--header 'Content-Type: application/json' \
--data '{
"name": "test_new"
}'

// получение информации о туре
curl --location 'http://localhost:8080/api/v1/tours/4'
