package com.ex.exex.config;

import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    private static final String DOCKET_API_BASE_PACKAGE = "com.ex.exex";

    @Bean
    public GroupedOpenApi paymentsApi() {
        return GroupedOpenApi.builder()
                .packagesToScan(DOCKET_API_BASE_PACKAGE)
                .group("exam")
                .pathsToMatch("/api/**")
                .build();
    }
}
