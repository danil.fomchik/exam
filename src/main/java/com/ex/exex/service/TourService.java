package com.ex.exex.service;

import com.ex.exex.dto.Tour;
import com.ex.exex.entity.TourSaveDto;
import com.ex.exex.entity.TourUpdateDto;
import com.ex.exex.repository.TourRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TourService {

    private final TourRepository tourRepository;
    private final ModelMapper modelMapper;

    public Long save(TourSaveDto tourSaveDto) {
        return tourRepository.save(modelMapper.map(tourSaveDto, Tour.class)).getId();
    }

    public void update(Long id, TourUpdateDto tourUpdateDto) {
        tourRepository.findById(id).ifPresentOrElse(tour ->
                {
                    modelMapper.map(tourUpdateDto, tour);
                    tourRepository.save(tour);
                },
                () -> {
                    throw new RuntimeException("tour not found");
                });
    }

    public Tour get(Long id) {
        return tourRepository.findById(id).orElseThrow(() -> new RuntimeException("tour not found"));
    }


    @Configuration
    public static class ModelMapperConfig {


        @Bean
        ModelMapper configure() {
            ModelMapper modelMapper = new ModelMapper();
            modelMapper.getConfiguration()
                    .setAmbiguityIgnored(true)
                    .setSkipNullEnabled(true);
            return modelMapper;
        }
    }
}

