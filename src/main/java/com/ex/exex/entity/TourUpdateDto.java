package com.ex.exex.entity;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class TourUpdateDto {

    private String name;
    private LocalDateTime arrivalDate;
    private LocalDateTime departureDate;

}
