package com.ex.exex.entity;

import io.swagger.v3.oas.annotations.Parameter;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Data
public class TourSaveDto {

    @NotBlank
    @Parameter(description = "name of the tour")
    private String name;
    @NotBlank
    @Parameter(description = "tour of the tour")
    private String destination;
    @NotBlank
    @Parameter(description = "arrival Date to the destination")
    private LocalDateTime arrivalDate;
    @NotBlank
    @Parameter(description = "departure Date Date to the destination")
    private LocalDateTime departureDate;

    @NotBlank
    @Parameter(description = "name of owner of the tour")
    private String ownerName;
    @NotBlank
    @Parameter(description = "surname of owner the tour")
    private String ownerSurname;
    @Min(18)
    @Max(100)
    private int ownerAge;
}
