package com.ex.exex.repository;

import com.ex.exex.dto.Tour;
import org.springframework.data.repository.CrudRepository;

public interface TourRepository extends CrudRepository<Tour,Long> {
}
