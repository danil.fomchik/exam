package com.ex.exex.controller;

import com.ex.exex.dto.Tour;
import com.ex.exex.entity.TourSaveDto;
import com.ex.exex.entity.TourUpdateDto;
import com.ex.exex.service.TourService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/tours")
@RequiredArgsConstructor
public class TourController {

    private final TourService tourService;

    @PostMapping
    public Long save(@RequestBody TourSaveDto tourSaveDto) {
        return tourService.save(tourSaveDto);
    }

    @PostMapping("/{id}")
    public void update(@PathVariable Long id, @RequestBody TourUpdateDto tourUpdateDto) {
        tourService.update(id, tourUpdateDto);
    }

    @GetMapping("/{id}")
    public Tour get(@PathVariable Long id) {
        return tourService.get(id);
    }
}
